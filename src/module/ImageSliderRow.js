

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  Image,
  Dimensions
} from 'react-native';
const { width } = Dimensions.get('window');


export default class ImageSliderRow extends React.Component {
  constructor(props) {
    super(props);
  
  }

  render() {
 
    return (

      <View style={styles.container}>
             <Text style={styles.textStyle}>{this.props.item.info.author_name}</Text>
            <Image style={styles.image} source={this.props.item.source} />
      </View>
    
    );
  }
}



const styles = StyleSheet.create({

    image: {
        width,
        height:width,
      },
      textStyle:{
        fontSize:20,
        color:"#000000",
        marginBottom:20
      },

      container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: StatusBar.currentHeight,
        marginBottom:20
    },
  
});