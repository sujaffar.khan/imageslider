import { StyleSheet, Platform,StatusBar,Dimensions} from "react-native";

const { width } = Dimensions.get('window');
const height = width * 1.2;

module.exports = StyleSheet.create({
    body: {
        backgroundColor:"#ffffff",
        flex:1,
        justifyContent:"center",
        flexDirection:'column',
        alignItems: "center"
        
      },
    
      textStyle:{
        fontSize:20,
        color:"#000000",
        marginBottom:20
      },
      container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: StatusBar.currentHeight,
    },
    
    scrollContainer: {
      height,
    },
    image: {
      width,
      height:width,
    },
});
