/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { View,RefreshControl,FlatList} from 'react-native';
import ImageSliderRow from'../../module/ImageSliderRow'
const styles = require("./style");


export default class HomeScreen extends React.Component {
    constructor() {
      super();
      var picNumber=1;
      this.state = {
        imageuri: '',
        items: [] ,
        refreshing: false,
        suffleArray:[]
        
      };
    
    }
  
  
    _onRefresh(){

      // pull to refresh       
      this.setState({
        refreshing: true,
      });

      this.prepareImgaeItems()
      console.log(JSON.stringify(this.state.items))

      }
  
    componentDidMount() {
      this.prepareImgaeItems()
    }
  
    prepareImgaeItems(){
      // preparing items array which iclude image uri and image info

      let items = Array.apply(null, Array(10)).map((v, i) => {
        //Loop to make image array to show in slider   
        const tt="Alejandro Escamilla";
        const tt1="Lukas Budimaier";
  
        return {
          source: {
            uri: i==0 ? 'https://picsum.photos/200/300?image=' + (i)    :  'https://picsum.photos/200/300?image=' + (i) 
          },
          info:{
            //id: i==0 ? tt : tt1
  
            author_name: this.getAuthorNmaeById(i+""),
            id: i==0 ? i : i
          }
  
        
        };
      });
      let suffleArray = this.doShuffle(items)
      this.setState({ 
        items,
        suffleArray,
        refreshing:false
      
      });
    }
  
   doShuffle(list) {
     // doing shuffle to load random images

      var ctr = list.length, temp, index;
  // While there are elements in the array
      while (ctr > 0) {
  // Pick a random index
          index = Math.floor(Math.random() * ctr);
  // Decrease ctr by 1
          ctr--;
  // And swap the last element with it
          temp = list[ctr];
          list[ctr] = list[index];
          list[index] = temp;
      }
      return list;
  }
  
  
  
  
    getAuthorNmaeById(id) {
    // getting author name by providing ids 

      switch(id) {
      case '0':
        return 'Alejandro Escamilla';
      case '1':
        return 'Alejandro Escamilla';  
      case '2':
        return 'Paul Jarvis';   
      case '3':
        return 'Tina Rataj'; 
      case '4':
        return 'Lukas Budimaier';   
      case '5':
        return 'Danielle MacInnes'; 
      case '6':
          return 'NASA'; 
      case '7':
          return 'E+N Photographies';   
      case '8':
          return 'Greg Rakozy';         
      case '9':
          return 'Matthew Wiebe'; 
      case '10':
          return 'Vladimir Kudinov';  
      case '11':
          return 'Benjamin Combs';    
      case '12':
          return 'Christopher Campbell';  
      case '13':
          return 'Christian Bardenhorst';     
      case '14':
            return 'Samantha Sophia';  
      case '15':
            return 'Roberto Nickson';  
      case '16':
              return 'Scott Webb';        
      case '17':
              return 'Cayton Heath';  
      case '18':
              return 'Oscar Keys'; 
      case '19':
              return 'Alexey Topolyanskiy';   
      }
    }

    render() {

      return (
  
        <View style={styles.container}>
            <FlatList
                        horizontal
                        onScroll={this.handleScroll}
                        pagingEnabled
                        refreshControl={
                          <RefreshControl
                          refreshing = {this.state.refreshing}
                          onRefresh={this._onRefresh.bind(this)}
                          />
                        }
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.suffleArray}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({item,index}) =>  
                        <ImageSliderRow item={item} index={index}>
                        </ImageSliderRow>
                    }
              />
        </View>
      );
    }
  }

 